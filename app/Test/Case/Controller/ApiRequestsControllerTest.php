<?php
App::uses('ApiRequestsController', 'Controller');

/**
 * ApiRequestsController Test Case
 */
class ApiRequestsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
    public $fixtures = array(
        'app.employee'
    );

    public function initialize() {  
        

    }
/**
 * testgetEmployee01 method
 * success 
 * @return void
 */
    public function testgetEmployee01() {
        $data['id'] = 1;
        $_SERVER['X-API-TOKEN'] = 'api_token_im_secured_promise';
        $result = $this->testAction('/api_requests/getEmployee', [
            'data' => $data, 
            'method' => 'get'
        ]);
        $result = json_decode($result, true);
        $expected = [
            'Employee' => [
                'id' => 1,
                'name' => 'Kevin Bernardo',
                'dept_id' => 1,
                'deleted' => 0,
                'created' => '2018-06-20',
                'modified' => '2018-06-20',
                'deleted_date' => null
            ]
        ];
        $this->assertEquals($expected, $result);
    }
/**
 * testgetEmployee02 method
 * failed with invalid token 
 * status 401
 * @return void
 */
    public function testgetEmployee02() {
        $data['id'] = 1;
        $_SERVER['X-API-TOKEN'] = 'secured_promise?';
        // $data['token'] = $this->token;
        $result = $this->testAction('/api_requests/getEmployee', [
            'data' => $data, 
            'method' => 'get'
        ]);
        $result = json_decode($result, true);
        $expected = [
            'Error' => [
                'status' => '401',
                'message' => __('Authentication Failed'),
            ]
        ];
        $this->assertEquals($expected, $result);
    }
/**
 * testgetEmployee03 method
 * failed wrong HTTP METHOD
 * status 405
 * @return void
 */
    public function testgetEmployee03() {
        $data['id'] = 1;
        $_SERVER['X-API-TOKEN'] = 'api_token_im_secured_promise';
        $result = $this->testAction('/api_requests/getEmployee', [
            'data' => $data, 
            'method' => 'DELETE'
        ]);
        $result = json_decode($result, true);
        $expected = [
            'Error' => [
                'status' => '405',
                'message' => __('Method Not Allowed'),
            ]
        ];
        $this->assertEquals($expected, $result);
    }
/**
 * testgetEmployee04 method
 * no parameter
 * @return void
 */
    public function testgetEmployee04() {
        $data['id'] = 1;
        $_SERVER['X-API-TOKEN'] = 'api_token_im_secured_promise';
        $result = $this->testAction('/api_requests/getEmployee', [
            'method' => 'get'
        ]);
        $result = json_decode($result, true);
        $expected = [
            'Error' => [
                'status' => '400',
                'message' => __('Invalid Parameter'),
            ]
        ];
        $this->assertEquals($expected, $result);
    }
/**
 * testgetEmployee05 method
 * no employee found
 * @return void
 */
    public function testgetEmployee05() {
        $data['id'] = 9999;
        $_SERVER['X-API-TOKEN'] = 'api_token_im_secured_promise';
        $result = $this->testAction('/api_requests/getEmployee', [
            'data' => $data,
            'method' => 'get'
        ]);
        $result = json_decode($result, true);
        $expected = [];
        $this->assertEquals($expected, $result);
    }
}
