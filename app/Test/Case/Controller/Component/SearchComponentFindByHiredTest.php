<?php
App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('ComponentCollection', 'Controller');
App::uses('SearchComponent', 'Controller/Component');

class SearchComponentFindByHiredTestController extends Controller {
}

class SearchComponentFindByHiredTest extends CakeTestCase {

    public $fixtures = [
        'app.employee',
    ];

    public $Controller = null;

    public function setUp() {
        parent::setUp();
        $Collection = new ComponentCollection();
        $this->SearchComponent = new SearchComponent($Collection);
        $CakeRequest = new CakeRequest();
        $CakeResponse = new CakeResponse();
        $this->Controller = new SearchComponentFindByHiredTestController($CakeRequest, $CakeResponse);
    }

    public function startup(Controller $controller) {        
        $this->Controller = $controller;
    }

/**
* get hired date with given date parameter 
*/
    public function testFindByHired01() {
        $date = '2018-06-20';
        $expected = [
            'Employee' => [
                'id' => 1,
                'name' => 'Kevin Bernardo',
                'dept_id' => 1,
                'deleted' => 0,
                'created' => '2018-06-20',
                'modified' => '2018-06-20',
                'deleted_date' => null
            ]
        ];
        $actual = $this->SearchComponent->findByHired($date);
        $this->assertEquals($expected, $actual);
    }
/**
* get hired date today
*/
    public function testFindByHired02() {
        // Mock June 28 2018 Current business day
        $current_business_day = '2018-06-28';
        $this->SearchComponent->Date = $this->getMock('Date', ['getCurrentBusinessDay']);
        $this->SearchComponent->Date->expects($this->any())->method('getCurrentBusinessDay')->will($this->returnValue($current_business_day));
        $expected = [
            'Employee' => [
                'id' => 4,
                'name' => 'Blances',
                'dept_id' => 1,
                'deleted' => 0,
                'created' => '2018-06-28',
                'modified' => '2018-06-28',
                'deleted_date' => null
            ]
        ];
        $actual = $this->SearchComponent->findByHired();
        $this->assertEquals($expected, $actual);
    }    

    public function tearDown() {
        parent::tearDown();
        unset($this->SearchComponent);
        unset($this->Controller);
    }
}