<?php
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('SearchComponent', 'Controller/Component');

/**
 * SearchComponent Test Case
 */
class SearchComponentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
    public $fixtures = array(
        'app.employee'
    );

/**
 * setUp method
 *
 * @return void
 */
    public function setUp() {
        parent::setUp();
        $Collection = new ComponentCollection();
        $this->Search = new SearchComponent($Collection);
    }

/**
 * tearDown method
 *
 * @return void
 */
    public function tearDown() {
        unset($this->Search);
        parent::tearDown();
    }

/**
 * testFindById01
 * Employee is exist
 * @return void
 */
    public function testFindById01() {
        $id = 1;
        $expected = [
            'Employee' => [
                'id' => 1,
                'name' => 'Kevin Bernardo',
                'dept_id' => 1,
                'deleted' => 0,
                'created' => '2018-06-20',
                'modified' => '2018-06-20',
                'deleted_date' => null
            ],
        ];
        $actual = $this->Search->findById($id);
        $this->assertEquals($expected, $actual);
    }
/**
 * testFindById02
 * Employee not exist
 * @return void
 */
    public function testFindById02() {
        $id = 9999999;
        $expected = [];
        $actual = $this->Search->findById($id);
        $this->assertEquals($expected, $actual);
    }
/**
 * testFindById03
 * Employee not exist
 * @return void
 */
    public function testFindById03() {
        $id = null;
        $expected = [];
        $actual = $this->Search->findById();
        $this->assertEquals($expected, $actual);
    }
/**
 * testFindById04
 * throw exception
 * @return void
 */
    public function testFindById04() {
        $id = '----!@aaaaa!a';
        try {
            $actual = $this->Search->findById($id);
            $this->fail();
        } catch (Exception $e) {
            $this->assertEqual(0, $e->getCode());
        }
    }
}
