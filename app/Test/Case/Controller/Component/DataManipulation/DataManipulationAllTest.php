<?php
App::uses('Controller', 'Controller');
class DataManipulationAllTest extends CakeTestSuite {
    public static function suite() {
        $suite = new CakeTestSuite('Data Manipulation tests');
        $suite->addTestDirectory(TESTS . 'Case' . DS . 'Controller' . DS . 'Component' . DS . 'DataManipulation' . DS);
        return $suite;
    }
}
