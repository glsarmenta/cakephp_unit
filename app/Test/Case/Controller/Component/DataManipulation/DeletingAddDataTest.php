<?php
App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('ComponentCollection', 'Controller');
App::uses('DataManipulationComponent', 'Controller/Component');
App::uses('SearchComponent', 'Controller/Component');

class DeletingAddDataTestController extends Controller {
}

class DeletingAddDataTest extends CakeTestCase {

    public $fixtures = [
        'app.employee',
    ];

    public $Controller = null;

    public function setUp() {
        parent::setUp();
        $Collection = new ComponentCollection();
        $this->DataManipulationComponent = new DataManipulationComponent($Collection);
        $this->SearchComponent = new SearchComponent($Collection);
        $CakeRequest = new CakeRequest();
        $CakeResponse = new CakeResponse();
        $this->Controller = new DeletingAddDataTestController($CakeRequest, $CakeResponse);

    }

    public function startup(Controller $controller) {        
        $this->Controller = $controller;
    }
/**
* this test is for : 
* Fixtures
* Mocking Component
* Nested Component function 
* Multi level function
* Transaction
*/
    public function testdeleteAddEmployee01() {
        $this->Employee = ClassRegistry::init('Employee');
        $preivous_count = $this->Employee->find('count');
        
        // Mock June 28 2018 Current business day
        $current_business_day = '2018-06-28';
        $previous_employees = $this->SearchComponent->findAllByHired($current_business_day);

        $this->DataManipulationComponent->Date = $this->getMock('Date', ['getCurrentBusinessDay','addMonth']);
        $this->DataManipulationComponent->Date->expects($this->any())
                            ->method('getCurrentBusinessDay')
                            ->will($this->returnValue($current_business_day));
        // Mock July 28 2018 1 month after of renewal
        $add_month = '2018-07-28';
        $this->DataManipulationComponent->Date->expects($this->any())
                            ->method('addMonth')
                            ->will($this->returnValue($add_month));

        $datasource = $this->Employee->getDataSource();
        try {
            if ($this->DataManipulationComponent->renewEmployee()) {
                $datasource->commit();
            } else {
                $datasource->rollback();    
            }
            
        } catch (Exception $e) {
            $datasource->rollback();
        }
        // check all record of 2018-06-28 changed to 2018-07-28
        $latest_employees = $this->SearchComponent->findAllByHired($add_month);
        $this->assertEquals(count($previous_employees), count($latest_employees));

        $after_function_count = $this->Employee->find('count');
        $this->assertEquals($preivous_count, $after_function_count);
    }
}