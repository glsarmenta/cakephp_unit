<?php
App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('ComponentCollection', 'Controller');
App::uses('DataManipulationComponent', 'Controller/Component');

class AddingDataTestController extends Controller {
}

class AddingDataTest extends CakeTestCase {

    public $fixtures = [
        'app.employee',
    ];

    public $Controller = null;

    public function setUp() {
        parent::setUp();
        $Collection = new ComponentCollection();
        $this->DataManipulationComponent = new DataManipulationComponent($Collection);
        $CakeRequest = new CakeRequest();
        $CakeResponse = new CakeResponse();
        $this->Controller = new AddingDataTestController($CakeRequest, $CakeResponse);

    }

    public function startup(Controller $controller) {        
        $this->Controller = $controller;
    }

/**
* 
*/
    public function testAddEmployee01() {
        $this->Employee = ClassRegistry::init('Employee');
        $data = [
            'Employee' => [
                'name' => 'new employee',
                'dept_id' => 1,
                'deleted' => 0,
                'created' => '2018-06-30',
                'modified' => '2018-06-30',
                'deleted_date' => null
            ]
        ];
        // We Expected that addNewEmployee will be success
        $expected = $this->Employee->find('count') + 1;
        $this->DataManipulationComponent->addNewEmployee($data);
        // count the latest record
        $actual = $this->Employee->find('count');
        $this->assertEquals($expected, $actual);
    }
/**
* 
*/
    public function testAddEmployee02() {
        $this->Employee = ClassRegistry::init('Employee');
        $data = [
            'Employee' => [
                'name' => 'new employee',
                'dept_id' => 'aaaa',
                'deleted' => 0,
                'created' => '2018-06-30',
                'modified' => '2018-06-30',
                'deleted_date' => null
            ]
        ];
        // We Expected that addNewEmployee will be success
        $expected = $this->Employee->find('count');
        $dataSource = $this->Employee->getDataSource();
        $dataSource->begin(); // opening of transaction
        try {
            $this->DataManipulationComponent->addNewEmployee($data);
            $name = $this->Employee->find('first', [
                        'order' => [
                            'id' => 'desc'
                        ]
                    ]); // Data was save during this function
            // Assert the latest record
            $this->assertContains('New Employee', $name['Employee']['name']);
            $this->fail(); // Manually fail the function
            $this->commit(); // commit all
        } catch (Exception $e) {
            $dataSource->rollback(); // rollback of transaction
        }
        $name = $this->Employee->find('first', [
                    'order' => [
                        'id' => 'desc'
                    ]
                ]); // Data was save during this function
        // Assert the latest record
        $this->assertContains('deleted user', $name['Employee']['name']);
        // count the latest record
        $actual = $this->Employee->find('count');
        $this->assertEquals($expected, $actual);
    }
}