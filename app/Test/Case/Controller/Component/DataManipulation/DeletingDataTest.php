<?php
App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('ComponentCollection', 'Controller');
App::uses('DataManipulationComponent', 'Controller/Component');

class DeletingDataTestController extends Controller {
}

class DeletingDataTest extends CakeTestCase {

    public $fixtures = [
        'app.employee',
    ];

    public $Controller = null;

    public function setUp() {
        parent::setUp();
        $Collection = new ComponentCollection();
        $this->DataManipulationComponent = new DataManipulationComponent($Collection);
        $CakeRequest = new CakeRequest();
        $CakeResponse = new CakeResponse();
        $this->Controller = new DeletingDataTestController($CakeRequest, $CakeResponse);
    }

    public function startup(Controller $controller) {        
        $this->Controller = $controller;
    }

    public function testdeleteEmployee01() {
        $this->Employee = ClassRegistry::init('Employee');
        $id = 1;
        // We Expected that deleteEmployee will be success
        $expected = $this->Employee->find('count') - 1;
        $this->DataManipulationComponent->deleteEmployee($id);
        // count the latest record
        $actual = $this->Employee->find('count');
        $this->assertEquals($expected, $actual);
    }

    public function testdeleteEmployee02() {
        $this->Employee = ClassRegistry::init('Employee');
        $id = 999;
        // We Expected that deleteEmployee will be success
        $expected = $this->Employee->find('count');
        $this->DataManipulationComponent->deleteEmployee($id);
        // count the latest record
        $actual = $this->Employee->find('count');
        // since id is not existing
        $this->assertEquals($expected, $actual);
    }
}