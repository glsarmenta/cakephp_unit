<?php
App::uses('EmployeesController', 'Controller');

/**
 * EmployeesController Test Case
 */
class EmployeesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.employee'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$result = $this->testAction('/employees/index');
		$this->assertInternalType('array', $this->vars['employees']);
	}
}
