<?php
/**
 * Employee Fixture
 */
class EmployeeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'dept_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'deleted' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'created' => array('type' => 'date', 'null' => true, 'default' => null),
		'modified' => array('type' => 'date', 'null' => true, 'default' => null),
		'deleted_date' => array('type' => 'date', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Kevin Bernardo',
			'dept_id' => 1,
			'deleted' => 0,
			'created' => '2018-06-20',
			'modified' => '2018-06-20',
			'deleted_date' => null
		),
		array(
			'id' => 2,
			'name' => 'Jodan Gallego',
			'dept_id' => 1,
			'deleted' => 0,
			'created' => '2018-06-21',
			'modified' => '2018-06-21',
			'deleted_date' => null
		),
		array(
			'id' => 3,
			'name' => 'Kelwin Ancheta',
			'dept_id' => 1,
			'deleted' => 0,
			'created' => '2018-05-10',
			'modified' => '2018-05-10',
			'deleted_date' => null
		),
		array(
			'id' => 4,
			'name' => 'Blances',
			'dept_id' => 1,
			'deleted' => 0,
			'created' => '2018-06-28',
			'modified' => '2018-06-28',
			'deleted_date' => null
		),
		array(
			'id' => 5,
			'name' => 'deleted user',
			'dept_id' => 1,
			'deleted' => 0,
			'created' => '2018-06-28',
			'modified' => '2018-06-28',
			'deleted_date' => '2018-06-28'
		),
	);

}
