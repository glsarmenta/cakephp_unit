<?php
App::uses('AppController', 'Controller');
/**
 * ApiRequests Controller
 *
 * @property ApiRequests $ApiRequests
 * @property PaginatorComponent $Paginator
 */
class ApiRequestsController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = ['Search'];
    public $autoRender = false;

    /**
     * beforeFilder method
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
    }

/**
 * index method
 *
 * @return void
 */
    public function getEmployee($id = null) {
        if ($this->request->header('X-API-TOKEN') !== 'api_token_im_secured_promise') {
            $this->response->statusCode(401);
            $errors =   [
                            'Error' =>  [
                                'status'  => 401,
                                'message' => __('Authentication Failed')
                            ]
                        ];
            return $this->response->body(json_encode($errors, JSON_UNESCAPED_UNICODE ));
        }

        if ($this->request->is('get')) {
            if (!isset($this->request->query['id'])) {
                $errors =   [
                            'Error' =>  [
                                'status'  => 400,
                                'message' => __('Invalid Parameter')
                            ]
                        ];
                return $this->response->body(json_encode($errors, JSON_UNESCAPED_UNICODE ));
            }
            if ($employee = $this->Search->findById($this->request->query['id'])) {
                return $this->response->body(json_encode($employee));
            }
            return $this->response->body(json_encode([]));
        }

        $errors =   [
                        'Error' =>  [
                            'status'  => 405,
                            'message' => __('Method Not Allowed')
                        ]
                    ];
        return $this->response->body(json_encode($errors, JSON_UNESCAPED_UNICODE ));

    }
}
