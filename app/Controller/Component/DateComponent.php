<?php
App::uses('Component', 'Controller');

class DateComponent extends Component {
    /**
    * Get current business day
    * @return Date Current business day
    */
    public function getCurrentBusinessDay() {
        return date('Y-m-d');
    }

    public function addMonth($date, $months_to_add) {
    	var_dump( date('Y-m-d', strtotime("+1 months", strtotime($date))));
    	return date('Y-m-d', strtotime("+1 months", strtotime($date)));
    }
}
