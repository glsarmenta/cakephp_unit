<?php
App::uses('Component', 'Controller');

class SearchComponent extends Component {
    public $components = [
        'Date',
    ];

    public function findById($id = null) {
        $this->Employee = ClassRegistry::init('Employee');
        return $this->Employee->findById($id);
    }
    
    public function findByHired($date = null) {
        if ($date === null) {
            $date = $this->Date->getCurrentBusinessDay();
        }
        $this->Employee = ClassRegistry::init('Employee');
        return $this->Employee->findByCreated($date);
    }

    public function findAllByHired($date = null) {
        if ($date === null) {
            $date = $this->Date->getCurrentBusinessDay();
        }
        $this->Employee = ClassRegistry::init('Employee');
        return $this->Employee->find('all', [
            'conditions' => [
                'created' => $date
            ]
        ]);
    }
}
