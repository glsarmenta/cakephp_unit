<?php
App::uses('Component', 'Controller');

class DataManipulationComponent extends Component {
    public $components = [
        'Date',
        'Search'
    ];

    public function addNewEmployee($data = []) {
        if (empty($data)) {
            return false;
        }
        $this->Employee = ClassRegistry::init('Employee');
        $data['Employee']['name'] = $this->capitalizeName($data['Employee']['name']);
        if ($this->Employee->save($data)) {
            $id = $this->Employee->getLastInsertID(); 
            $this->Employee->clear();
            return $this->Search->findById($id);
        }
        return false;
    }

    public function deleteEmployee($id) {
        $this->Employee = ClassRegistry::init('Employee');
        if ($this->Search->findById($id)) {
            if ($this->Employee->delete($id)) {
                return true;
            }
        }
        return false;
    }

    public function renewEmployee() {
        $today = $this->Date->getCurrentBusinessDay();
        if ($employees = $this->Search->findAllByHired($today)) {
            $save_flag = true;
            foreach ($employees as $key => $value) {
                if ($this->deleteEmployee($value['Employee']['id'])) {
                    $value['Employee']['created'] = $this->Date->addMonth($today, 1);
                    if (!$this->addNewEmployee($value)) {
                        $save_flag = false;
                        break;
                    }
                }
            }
            return $save_flag;
        }
        return false;
    }
    public function capitalizeName($name = '') {
        return ucwords($name);
    }
}
